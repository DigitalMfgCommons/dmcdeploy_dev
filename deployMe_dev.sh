#!/bin/bash

### useful aliases
alias la='ls -lAh'


## zshell
sudo yum install -y zsh
curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | bash
#zsh

###install the tooling

##terraform

cd /tmp
mkdir ~/keys
#download terraform
 wget https://releases.hashicorp.com/terraform/0.6.8/terraform_0.6.8_linux_amd64.zip

mkdir tf
cd tf
unzip ../*.zip
cd ..
# moving terraform to home dir
mv tf ~

#add tf to path
echo "export PATH=$PATH:/home/ec2-user/tf" >> ~/.bashrc
source ~/.bashrc

# test if properly installed
terraform -v

##git
sudo yum install git -y
sudo yum install git-daemon -y





###get the deployment repos

cd ~
git clone https://bitbucket.org/DigitalMfgCommons/dmcfrontend.git

git clone https://bitbucket.org/DigitalMfgCommons/dmcrestservices.git

git clone https://bitbucket.org/DigitalMfgCommons/dmcdb.git

git clone https://bitbucket.org/DigitalMfgCommons/dmcdomeserver.git

git clone https://bitbucket.org/DigitalMfgCommons/dmcactivemq.git

git clone https://bitbucket.org/DigitalMfgCommons/dmcsolr.git

git clone https://bitbucket.org/DigitalMfgCommons/dmcdeploy.git

git clone https://bitbucket.org/DigitalMfgCommons/dmcdeploy_dev.git

####--------------
alias tf='terraform'
mv /tmp/deployMe_DevMakina.sh ~
./deployMe_DevMakina.sh	
