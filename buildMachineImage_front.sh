#!/bin/bash -v

#anything printed on stdout and stderr to be sent to the syslog1, as well as being echoed back to the original shell’s stderr.
exec 1> >(logger -s -t $(basename $0)) 2>&1

function buildAMIBase {
    #install needed packages
    sudo yum remove sendmail -y
    sudo yum install httpd -y
    sudo yum install php php-pgsql -y
    sudo yum install wget -y
    sudo yum install git -y

    # get Shibbolth service provider install script
    git clone https://bitbucket.org/DigitalMfgCommons/dmcfrontend.git
    # move Shibbolth service provider install script
    mv dmcfrontend/install_ShibbolthSpDependencies.sh .
    # move Shibbolth configuration files
    mv dmcfrontend/configurationFiles .

    # install Shibbolth service provider and dependencies
    ./install_ShibbolthSpDependencies.sh
    configureShibbolethServiceProvider
}

function configureShibbolethServiceProvider {
    # configure SP:

    # edit /etc/sysconfig/httpd
    sudo su -c "echo \"export LD_LIBRARY_PATH=/opt/shibboleth-sp/lib\" >>  /etc/sysconfig/httpd"
    
    # copy shibboleth SP Apache configuration to apache conf.d directory
    apacheConfigDir=configurationFiles/apache/version2.2
    sudo -u root -E cp $apacheConfigDir/apache22.conf /etc/httpd/conf.d/apache22.conf

    # copy shibboleth SP configuration files to shibboleth SP configuration directory
    shibSPConfigDir=configurationFiles/shibbolethSP/version2.5.5
    sudo -u root -E cp $shibSPConfigDir/attribute-map.xml /opt/shibboleth-sp/etc/shibboleth/attribute-map.xml
    sudo -u root -E cp $shibSPConfigDir/shibboleth2.xml /opt/shibboleth-sp/etc/shibboleth/shibboleth2.xml
    sudo -u root -E cp $shibSPConfigDir/CirrusIdentitySocialProviders-metadata.xml /opt/shibboleth-sp/etc/shibboleth/CirrusIdentitySocialProviders-metadata.xml
}

##command to create the AMI base
buildAMIBase

# remove unneeded software

# create option to makeAMISnapshot
