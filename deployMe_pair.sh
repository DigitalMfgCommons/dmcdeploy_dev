sudo yum install wget -y
sudo yum install unzip -y
sudo yum install git -y
sudo yum install nano -y
sudo yum install tmux -y

mkdir ~/repo
cd ~/repo
git clone https://bitbucket.org/DigitalMfgCommons/dmcpair.git
git clone https://bitbucket.org/DigitalMfgCommons/dmcdeploy_dev.git


mkdir ~/tf
cd ~/tf
wget https://releases.hashicorp.com/terraform/0.6.6/terraform_0.6.6_linux_amd64.zip
unzip terraform_0.6.6_linux_amd64.zip


